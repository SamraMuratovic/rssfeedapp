//
//  JsonManager.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation

struct JSONManager {
    
    var plistName: String
    
    private var plistPath: String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectory: String = paths[0] 
        return (documentDirectory as NSString).appendingPathComponent("\(plistName).plist")
    }
    
    func load<T: Decodable & Encodable>() -> [T]? {
        let fileManager: FileManager = FileManager.default
        
        if fileManager.fileExists(atPath: plistPath) {
            do {
                let data = try PlistReader<[T]>(PlistReader.Source.savedPlist(plistPath))
                return data.data
            } catch let err {
                print(err.localizedDescription)
            }
        }
        
        return nil
    }
    
    func save<T: Encodable>(data: [T]) {
        let plistURL = URL(fileURLWithPath: plistPath)
        
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        do {
            let data = try encoder.encode(data)
            try data.write(to: plistURL)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func delete() {
        let plistURL = URL(fileURLWithPath: plistPath)
        let fileManager: FileManager = FileManager.default
        
        try? fileManager.removeItem(at: plistURL)
    }
    
}


public class PlistReader<Value: Codable> {

    public enum Errors: Error {
        case fileNotFound
    }

    public enum Source {
        case infoPlist(_: Bundle)
        case plist(_: String, _: Bundle)
        case savedPlist(_: String)
        
        internal func data() throws -> Data {
            switch self {
            case .infoPlist(let bundle):
                guard let infoDict = bundle.infoDictionary else {
                    throw Errors.fileNotFound
                }
                return try JSONSerialization.data(withJSONObject: infoDict)
            case .plist(let filename, let bundle):
                guard let path = bundle.path(forResource: filename, ofType: "plist") else {
                    throw Errors.fileNotFound
                }
                return try Data(contentsOf: URL(fileURLWithPath: path))
            case .savedPlist(let filename):
                return try Data(contentsOf: URL(fileURLWithPath: filename))
            }
        }
    }

    public let data: Value

    public init(_ file: PlistReader.Source = .infoPlist(Bundle.main)) throws {
        let rawData = try file.data()
        let decoder = PropertyListDecoder()
        self.data = try decoder.decode(Value.self, from: rawData)
    }

}
