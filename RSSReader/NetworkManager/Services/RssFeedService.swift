//
//  RssFeedService.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation
import FeedKit

protocol RssFeedServiceDelegate: ServiceDelegate {}

class RssFeedService {
    
    weak var delegate: RssFeedServiceDelegate?
    
    init(delegate: RssFeedServiceDelegate?) {
        self.delegate = delegate
    }
    
    func getRSSFeeds(url: String) {
        FeedKitManager.requestRSSFeed(stringURL: url, withSucces: { [weak self] (data: RSSFeed?) in
            var feedModel: FeedModel?
            if let rssFeed = data {
                feedModel = FeedModel(title: rssFeed.title ?? "",
                                      link: rssFeed.link ?? "",
                                      description: rssFeed.description ?? "",
                                      image: rssFeed.image?.url ?? "",
                                      items: [])
                
                if let items = rssFeed.items {
                    feedModel?.items = items.map({ item -> ItemModel in
                        return ItemModel(title: item.title ?? "",
                                         link: item.link ?? "",
                                         description: item.description ?? "",
                                         image: item.media?.mediaThumbnails?[0].attributes?.url ??
                                            item.enclosure?.attributes?.url ??
                                            item.media?.mediaContents?[0].attributes?.url ??
                                         "")
                    })
                }
                self?.delegate?.success(data: feedModel)
            } else {
                self?.delegate?.error(message: "Coud not parse any data for this link")
            }
        }, withError: { [weak self] error in
            self?.delegate?.error(message: error)
        })
    }
}
