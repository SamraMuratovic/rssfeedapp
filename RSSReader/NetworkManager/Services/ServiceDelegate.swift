//
//  ServiceDelegate.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation

protocol ServiceDelegate: AnyObject {
    func error(message: String)
    func success(data: Any?)
}
