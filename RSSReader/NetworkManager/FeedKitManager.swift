//
//  FeedKitManager.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation
import FeedKit

// MARK: FeedKitManager
class FeedKitManager {
    
    static let instance = FeedKitManager()
    
    static func requestRSSFeed(stringURL: String,
                               withSucces succesHandler: @escaping (_ data: RSSFeed?) -> Void,
                               withError errorHandler: @escaping (String) -> Void) {
        if let url = URL(string: stringURL) {
            let parser = FeedParser(URL: url)
            parser.parseAsync { (result) in
                switch result {
                case .success(let feed):
                    DispatchQueue.main.async {
                        succesHandler(feed.rssFeed)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        errorHandler(error.failureReason ?? "Could not parse URL")
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                errorHandler("Bad URL")
            }
        }
    }
    
}
