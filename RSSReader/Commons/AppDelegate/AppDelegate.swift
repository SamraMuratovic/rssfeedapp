//
//  AppDelegate.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var rootController: UINavigationController {
        guard let window = UIApplication.shared.delegate?.window else { return UINavigationController() }
        window?.makeKeyAndVisible()
        return window?.rootViewController as? UINavigationController ?? UINavigationController()
    }
    
    private lazy var applicationCoordinator: Coordinator = self.makeCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        applicationCoordinator.start()
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        return true
    }
    
    // MARK: Coordinator
    private func makeCoordinator() -> Coordinator {
       return ApplicationCoordinator(
           router: RouterImp(childRootController: self.rootController),
           coordinatorFactory: CoordinatorFactoryImp()
       )
    }
}
