//
//  AppCoordinator.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

final class ApplicationCoordinator: BaseCoordinator {
    
    private let router: Router
    private let coordinatorFactory: CoordinatorFactory
    
    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        runFeedsFlow()
    }
    
    private func runFeedsFlow() {
        let coordinator = coordinatorFactory.makeFeedsCoordinator(router: router)
        addDependency(coordinator)
        coordinator.start()
    }
}
