//
//  CoordinatorFactory.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

protocol CoordinatorFactory {
    func makeFeedsCoordinator(router: Router) -> Coordinator
}
