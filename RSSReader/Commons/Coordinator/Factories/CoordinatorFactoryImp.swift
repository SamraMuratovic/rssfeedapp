//
//  CoordinatorFactoryImp.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation
import UIKit

final class CoordinatorFactoryImp {}

extension CoordinatorFactoryImp: CoordinatorFactory {
    
    func makeFeedsCoordinator(router: Router) -> Coordinator {
        return FeedsCoordinator(router: router, factory: ModuleFactoryImp())
    }
    
    private func router(_ navController: UINavigationController?) -> Router {
        return RouterImp(childRootController: navigationController(navController))
    }
    
    private func navigationController(_ navController: UINavigationController?) -> UINavigationController {
        if let navController = navController {
            return navController
        } else { return UINavigationController.controllerFromStoryboard(.main)}
    }
    
}
