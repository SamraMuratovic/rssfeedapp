//
//  FeedsModuleFactory.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

protocol FeedsModuleFactory {
    func makeFeedsOutput() -> FeedsView
    func makeAddFeedOutout() -> AddFeedView
    func makeItemsOutput(model: LocalFeedModel) -> BackView & AlertView
}
