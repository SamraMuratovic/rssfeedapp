//
//  ModuleFactoryImp.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

protocol BaseView: NSObjectProtocol, Presentable {}

protocol AlertView: BaseView {
    var onShowAlerView: ((String) -> Void)? { get set }
}

protocol BackView: BaseView {
    var onBack: (() -> Void)? { get set }
}

final class ModuleFactoryImp {}

extension ModuleFactoryImp: FeedsModuleFactory {
    
    func makeFeedsOutput() -> FeedsView {
        return FeedsViewController.controllerFromStoryboard(.main)
    }
    
    func makeAddFeedOutout() -> AddFeedView {
        return AddFeedViewController.controllerFromStoryboard(.main)
    }
    
    func makeItemsOutput(model: LocalFeedModel) -> BackView & AlertView {
        let viewC = ItemsViewController.controllerFromStoryboard(.main)
        viewC.viewModel = ItemsViewModel(model: model)
        
        return viewC
    }
}
