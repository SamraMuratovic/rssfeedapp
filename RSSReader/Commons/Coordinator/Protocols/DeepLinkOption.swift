//
//  DeepLinkOption.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

struct DeepLinkURLConstants {
    static let main = "main"
}

enum DeepLinkOption {
    case main
    
    static func build(with dict: [String: AnyObject]?) -> DeepLinkOption? {
        guard let launchId = dict?["launch_id"] as? String else { return nil }
            
        switch launchId {
        case DeepLinkURLConstants.main: return .main
        default: return nil
        }
    }
}
