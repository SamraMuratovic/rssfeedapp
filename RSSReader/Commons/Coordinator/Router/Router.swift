//
//  Router.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation
import UIKit

protocol Router: Presentable {
    
    func present(_ module: Presentable?)
    func present(_ module: Presentable?, animated: Bool, style: UIModalPresentationStyle)
    func presentController(_ controller: UIViewController?, animated: Bool)
    func presentOnTop(_ module: Presentable?, animated: Bool)

    func push(_ module: Presentable?)
    func push(_ module: Presentable?, animated: Bool)
    func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?)
    
    func pushOnChild(_ module: Presentable?)
    func pushOnChild(_ module: Presentable?, animated: Bool)
    func pushOnChild(_ module: Presentable?, animated: Bool, completion: (() -> Void)?)

    func popModule()
    func popModule(animated: Bool)
    
    func popModuleFromChild()
    func popModuleFromChild(animated: Bool)
    
    func dismissModule()
    func dismissModule(animated: Bool, completion: (() -> Void)?)
    func dismissModuleOnTop()

    func setRootModule(_ module: Presentable?)
    func setRootModule(_ module: Presentable?, hideBar: Bool)
    
    func popToRootModule(animated: Bool)
    
}
