//
//  RouterImp.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation
import UIKit

final class RouterImp: NSObject, Router {
    
    private weak var baseRootController: UINavigationController?
    private weak var childRootController: UINavigationController?
    
    private var completions: [UIViewController : () -> Void]
    
    init(childRootController: UINavigationController) {
        self.childRootController = childRootController
        let keyWindow = UIApplication.shared.windows
            .filter({$0.isKeyWindow}).first
        if let baseC = keyWindow?.rootViewController as? UINavigationController {
            baseRootController = baseC
        }
        completions = [:]
    }
    
    func toPresent() -> UIViewController? {
        return childRootController
    }
    
    func present(_ module: Presentable?) {
        present(module, animated: true)
    }
    
    func present(_ module: Presentable?, animated: Bool, style: UIModalPresentationStyle = .overFullScreen) {
        guard let controller = module?.toPresent() else { return }
        controller.modalPresentationStyle = style
        childRootController?.present(controller, animated: animated, completion: nil)
    }
    
    func presentController(_ controller: UIViewController?, animated: Bool) {
        guard let controller = controller  else { return }
        controller.modalPresentationStyle = .overFullScreen
        childRootController?.present(controller, animated: animated, completion: nil)
    }
    
    func presentOnTop(_ module: Presentable?, animated: Bool) {
        guard let controller = module?.toPresent() else { return }
        controller.modalPresentationStyle = .overFullScreen
        childRootController?.presentedViewController?.present(controller, animated: animated, completion: nil)
    }
    
    func dismissModule() {
        dismissModule(animated: true, completion: nil)
    }
    
    func dismissModule(animated: Bool, completion: (() -> Void)?) {
        childRootController?.dismiss(animated: animated, completion: completion)
    }
    
    func dismissModuleOnTop() {
        childRootController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func push(_ module: Presentable?) {
        push(module, animated: true)
    }
    
    func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
        guard
            let controller = module?.toPresent(),
            (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }
        
        if let completion = completion {
            completions[controller] = completion
        }
        baseRootController?.pushViewController(controller, animated: animated)
    }
    
    func pushOnChild(_ module: Presentable?) {
        pushOnChild(module, animated: true)
    }
    
    func pushOnChild(_ module: Presentable?, animated: Bool) {
        pushOnChild(module, animated: animated, completion: nil)
    }
    
    func pushOnChild(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
        guard
            let controller = module?.toPresent(),
            (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }
        
        if let completion = completion {
            completions[controller] = completion
        }
        childRootController?.pushViewController(controller, animated: animated)
    }
    
    func popModule() {
        popModule(animated: true)
    }
    
    func popModule(animated: Bool) {
        if let controller = baseRootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func popModuleFromChild() {
        popModuleFromChild(animated: true)
    }
    
    func popModuleFromChild(animated: Bool) {
        if let controller = childRootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func setRootModule(_ module: Presentable?) {
        setRootModule(module, hideBar: false)
    }
    
    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        guard let controller = module?.toPresent() else { return }
        childRootController?.setViewControllers([controller], animated: false)
        childRootController?.isNavigationBarHidden = hideBar
    }
    
    func popToRootModule(animated: Bool) {
        if let controllers = baseRootController?.popToRootViewController(animated: animated) {
            controllers.forEach { controller in
                runCompletion(for: controller)
            }
        }
    }
    
    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
    
}
