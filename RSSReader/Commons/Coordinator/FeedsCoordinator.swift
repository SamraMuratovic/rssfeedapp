//
//  FeedsCoordinator.swift
//  RSSReader
//
//  Created by Samra Muratovic on 5. 7. 2021..
//

import Foundation

class FeedsCoordinator: BaseCoordinator {
    
    private let factory: FeedsModuleFactory
    private let router: Router
    
    init(router: Router, factory: FeedsModuleFactory) {
        self.factory = factory
        self.router = router
    }
    
    override func start() {
       showFeedsOutput()
    }
    
    private func showFeedsOutput() {
        let output = factory.makeFeedsOutput()
        output.onAddNewFeed = { [weak self] in
            self?.showAddNewFeedOutput()
        }
        output.onShowAlerView = { [weak self] message in
            guard let sSelf = self else { return }
            sSelf.showAlert(message: message, router: sSelf.router)
        }
        output.onShowItems = { [weak self] model in
            self?.onItemsOutput(model)
        }
        router.setRootModule(output, hideBar: true)
    }
    
    private func showAddNewFeedOutput() {
        let output = factory.makeAddFeedOutout()
        output.onBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        output.onShowAlerView = { [weak self] message in
            guard let sSelf = self else { return }
            sSelf.showAlert(message: message, router: sSelf.router)
        }
        router.push(output, animated: true)
    }
    
    private func onItemsOutput(_ model: LocalFeedModel) {
        let output = factory.makeItemsOutput(model: model)
        output.onBack = { [weak self] in
            self?.router.popModule()
        }
        output.onShowAlerView = { [weak self] message in
            guard let sSelf = self else { return }
            sSelf.showAlert(message: message, router: sSelf.router)
        }
        router.push(output)
    }
}
