//
//  ColorHelper.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import UIKit

enum ColorHelper: String {
    
    case bgOrange
    case bgOrangeCell
    
    func color() -> UIColor {
        return UIColor(named: self.rawValue)!
    }
}
