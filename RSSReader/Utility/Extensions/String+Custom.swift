//
//  String+Custom.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation
import UIKit

extension String {
    func openUrl() {
        if let url = URL(string: self) {
            DispatchQueue.main.async {
                UIApplication.shared.open(url)
            }
        }
    }
}
