//
//  NSObject+Custom.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

extension NSObject {
    
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
}
