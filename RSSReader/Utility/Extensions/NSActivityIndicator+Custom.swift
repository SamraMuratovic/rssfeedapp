//
//  NSActivityIndicator+Custom.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation
import NVActivityIndicatorView

extension NVActivityIndicatorViewable where Self: UIViewController {
    
    public func showLoader() {
        startAnimating(CGSize(width: 80, height: 80),
                       message: "",
                       messageFont: nil,
                       type: .ballBeat,
                       color: nil,
                       padding: 0.0,
                       displayTimeThreshold: 0,
                       minimumDisplayTime: 0,
                       backgroundColor: nil,
                       textColor: nil)
    }
    
    public func hideLoader() {
        stopAnimating()
    }
}
