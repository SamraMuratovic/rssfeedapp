//
//  Constants.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

struct Constants {

    struct Device {
        static let width = UIScreen.main.bounds.width
        static let height = UIScreen.main.bounds.height
        static let smallerScreens = (Device.height <= 568.0)
        static let iphone6Size = (Device.height >= 667.0)
        static let bigerScreens = (Device.height >= 800.0)
    }
    
}
