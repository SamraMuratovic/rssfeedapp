//
//  EmptyDataCell.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import UIKit

class ImageCell: BaseRxCell {
    
    struct Params {
        static let image = "image"
    }
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func configureCell(withData data: [String: Any]) {
        if let image = data[Params.image] as? UIImage {
            imgView.image = image
        }
    }
}
