//
//  ActivityIndicatorPresenter.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation
import UIKit

public protocol ActivityIndicatorPresenter {
    
    var activityIndicator: UIActivityIndicatorView { get }
    func showLoader()
    func hideLoader()
    
}
