//
//  SearchView.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import UIKit
import RxCocoa
import RxSwift

class SearchView: UIView {
    
    private var nibName = "SearchView"

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnSearchDelete: UIButton!
    
    var searchedText: BehaviorRelay<String> = BehaviorRelay(value: "")
    var disposeBag = DisposeBag()
    var searchTapped: PublishSubject<Void> = PublishSubject()
    var deleteText: PublishSubject<Void> = PublishSubject()
    
    var firstResponder: Bool = false
    var placeHolderText: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    convenience init(placeHolderText: String = "",
                     firstResponder: Bool = false,
                     frame: CGRect = CGRect(x: 20,
                                            y: 0,
                                            width: 40,
                                            height: 55)) {
        self.init(frame: frame)
        self.placeHolderText = placeHolderText
        self.firstResponder = firstResponder
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(contentView)
        
        txtInput.delegate = self

        txtInput.placeholder = placeHolderText
        txtInput.rx.text.orEmpty.bind(to: searchedText).disposed(by: disposeBag)
        searchedText.asObservable().subscribe(onNext: { [weak self] txt in
            self?.txtInput.text = txt
            self?.btnSearchDelete.isHidden = txt.isEmpty
        }).disposed(by: disposeBag)
        if firstResponder {
            txtInput.becomeFirstResponder()
        }

    }
    
    @IBAction func btnDeleteTap(_ sender: Any) {
        txtInput.text = ""
        btnSearchDelete.isHidden = true
        deleteText.onNext(())
    }
}

extension SearchView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        txtInput.placeholder = nil
        btnSearchDelete.isHidden = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchedText.accept(textField.text ?? "")
        textField.resignFirstResponder()
        if textField.text?.count ?? 0 < 3 {
            textField.becomeFirstResponder()
            return true
        }
        searchTapped.onNext(())
        return true
    }
    
}
