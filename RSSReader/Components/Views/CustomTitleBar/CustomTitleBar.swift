//
//  CustomTitleBar.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit
import RxCocoa
import RxSwift

enum CustomTitleType {
    case leftButtonTitle
    case titleRightButton
    case full
}

class CustomTitleBar: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var topLabelOffset: NSLayoutConstraint!
    
    private var type: CustomTitleType = .leftButtonTitle
    private var leftImage = UIImage()
    private var rightImage = UIImage()
    private var title = ""
    
    var leftButtonTapped: PublishSubject<Void> = PublishSubject()
    var rightButtonTapped: PublishSubject<Void> = PublishSubject()
    var disposeBag = DisposeBag()
    
    convenience init(type: CustomTitleType,
                     leftBottonImage: UIImage? = UIImage(named: "ic_back"),
                     rightButtonImage: UIImage? = nil,
                     title: String = "",
                     subtitle: String = "") {
        let customFrame = CGRect(x: 0, y: 0, width: Constants.Device.width, height: 70)
        self.init(frame: customFrame)
        self.type = type
        self.leftImage = leftBottonImage ?? UIImage()
        self.rightImage = rightButtonImage ?? UIImage()
        self.title = title
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomTitleBar", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(contentView)
        
        btnLeft.setImage(leftImage, for: .normal)
        btnRight.setImage(rightImage, for: .normal)
        lblTitle.text = title
        switch type {
        case .leftButtonTitle:
            btnRight.isHidden = true
        case .titleRightButton:
            btnLeft.isHidden = true
        case .full:
            topLabelOffset.constant = -10
        }
        
        setupBindables()
    }
    
    private func setupBindables() {
        btnRight.rx.tap.bind(to: rightButtonTapped).disposed(by: disposeBag)
        btnLeft.rx.tap.bind(to: leftButtonTapped).disposed(by: disposeBag)
    }
}
