//
//  FeedCellTableViewCell.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher

struct FeedCellBindable: CellDelegate {
    var rightBtnTap: PublishSubject<Void> = PublishSubject()
    var cellTap: PublishSubject<Void> = PublishSubject()
}

class FeedCellTableViewCell: BaseRxCell {

    struct Params {
        static let titleText = "titleText"
        static let titleColor = "titleColor"
        static let descriptionText = "descriptionText"
        static let descriptionColor = "descriptionColor"
        static let imageUrl = "imageUrl"
        static let hasRightButton = "hasRightButton"
        static let rightButtonImage = "rightButtonImage"
    }
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnHelper: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var holderView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgView.kf.cancelDownloadTask()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.roundCorners(radius: 20.0)
    }
    
    override func configureCell(withViewModel: Any?) {
        guard let vM = withViewModel as? FeedCellBindable else { return }
        btnHelper.rx.tap.bind(to: vM.cellTap).disposed(by: disposeBag)
        btnRight.rx.tap.bind(to: vM.rightBtnTap).disposed(by: disposeBag)
    }
    
    override func configureCell(withData data: [String: Any]) {
        if let urlString = data[Params.imageUrl] as? String, let url = URL(string: urlString) {
            imgView.kf.setImage(
                with: url,
                placeholder: UIImage(named: "ic_no_image"),
                options: [.onlyLoadFirstFrame]
            )
        } else {
            imgView.image = UIImage(named: "ic_no_image")
        }
        lblTitle.text = data[Params.titleText] as? String ?? ""
        lblDescription.text = data[Params.descriptionText] as? String ?? ""
        lblTitle.textColor = data[Params.titleColor] as? UIColor ?? .white
        lblDescription.textColor = data[Params.descriptionColor] as? UIColor ?? .white
        if let hasRightBtn = data[Params.hasRightButton] as? Bool {
            btnRight.isHidden = !hasRightBtn
        }
        if let image = data[Params.rightButtonImage] as? UIImage {
            btnRight.setImage(image, for: .normal)
        }
    }
}
