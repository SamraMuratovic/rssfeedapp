//
//  StoryboardsEnum.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

enum Storyboards: String {
    
    case main = "Main"
    case home = "Home"
    
}
