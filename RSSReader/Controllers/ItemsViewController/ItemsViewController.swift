//
//  ItemsViewController.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import UIKit

class ItemsViewController: RxBaseListViewController, BackView, AlertView {

    var onBack: (() -> Void)?
    var viewModel: ItemsViewModel?
    var onShowAlerView: ((String) -> Void)?
    
    @IBOutlet weak var titleBar: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleBar = CustomTitleBar(
            type: .leftButtonTitle,
            title: viewModel?.model?.title ?? "Items"
        )
        
        titleBar.leftButtonTapped.asObservable()
            .subscribe(onNext: { [weak self] in
            self?.onBack?()
        }).disposed(by: disposeBag)
        
        self.titleBar.addSubview(titleBar)
        
        viewModel?.onAlert.asObservable()
            .subscribe(onNext: { [weak self] error in
            self?.onShowAlerView?(error)
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard  let vM = viewModel else { return }
        vM.loadItems()
    }
    
    override func baseViewModel() -> RxBaseViewControllerDelegate? {
        return viewModel
    }
}
