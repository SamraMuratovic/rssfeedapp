//
//  ItemsViewModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation
import RxSwift
import RxCocoa

class ItemsViewModel: RxBaseViewControllerDelegate {
    
    var disposeBag: DisposeBag = DisposeBag()
    var screenState: BehaviorRelay<[SectionModel]> = BehaviorRelay(value: [])
    var onShowLoader: (() -> Void)?
    var onStopLoader: (() -> Void)?
    var section: SectionModel = SectionModel(items: [])
    var onAlert: PublishSubject<String> = PublishSubject()
    
    private var service: RssFeedService = RssFeedService(delegate: nil)
    private var items: BehaviorRelay<[ItemModel]> = BehaviorRelay(value: [])
    var model: LocalFeedModel?

    init(model: LocalFeedModel) {
        self.model = model
        service.delegate = self
        setupBindables()
    }
    
    func loadItems() {
        onShowLoader?()
        service.getRSSFeeds(url: model?.rssLink ?? "")
    }
    
    private func setupBindables() {
        items.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.updateView()
            }).disposed(by: disposeBag)
    }
    
    private func updateView() {
        section.items.removeAll()
        
        for item in items.value {
            let feedCellBindable = FeedCellBindable()
            
            section.items.append(CellType(reuseID: FeedCellTableViewCell.reuseID,
                                          data: [
                                            FeedCellTableViewCell.Params.titleText: item.title,
                                            FeedCellTableViewCell.Params.descriptionText: item.description,
                                            FeedCellTableViewCell.Params.imageUrl: item.image,
                                            FeedCellTableViewCell.Params.hasRightButton: false
                                          ],
                                          vM: feedCellBindable, height: UITableView.automaticDimension))
            
            feedCellBindable.cellTap.asObservable()
                .subscribe(onNext: { _ in
                    item.link.openUrl()
                }).disposed(by: disposeBag)
        }
        screenState.accept([section])
        
    }
}

extension ItemsViewModel: RssFeedServiceDelegate {
    func error(message: String) {
        onStopLoader?()
        onAlert.onNext(message)
    }
    
    func success(data: Any?) {
        onStopLoader?()
        if let response = data as? FeedModel {
            items.accept(response.items ?? [])
        }
    }
}
