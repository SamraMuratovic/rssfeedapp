//
//  AddFeedViewController.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

protocol AddFeedView: AlertView, BackView {
    var onAddNewFeed: (() -> Void)? { get set }
}

class AddFeedViewController: RxBaseListViewController, AddFeedView {
    
    var onBack: (() -> Void)?
    var onAddNewFeed: (() -> Void)?
    var onShowAlerView: ((String) -> Void)?

    @IBOutlet weak var titleBar: UIView!
    
    let viewModel: AddFeedViewModel = AddFeedViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleBar = CustomTitleBar(
            type: .leftButtonTitle,
            title: "ADD NEW FEED"
        )
        
        titleBar.leftButtonTapped.asObservable()
            .subscribe(onNext: { [weak self] in
            self?.onBack?()
        }).disposed(by: disposeBag)
        
        self.titleBar.addSubview(titleBar)
        
        viewModel.onAlert.asObservable()
            .subscribe(onNext: { [weak self] error in
            self?.onShowAlerView?(error)
        }).disposed(by: disposeBag)
        
        viewModel.onConfirmAction = { [weak self] actionSheet in
            DispatchQueue.main.async {
                self?.present(actionSheet, animated: true, completion: nil)
            }
        }
    }
    
    override func baseViewModel() -> RxBaseViewControllerDelegate? {
        return viewModel
    }
}
