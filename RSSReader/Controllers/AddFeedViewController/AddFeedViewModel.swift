//
//  AddFeedViewModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation
import RxCocoa
import RxSwift
import FeedKit

class AddFeedViewModel: RxBaseViewControllerDelegate {
    
    var disposeBag: DisposeBag = DisposeBag()
    var screenState: BehaviorRelay<[SectionModel]> = BehaviorRelay(value: [])
    var onShowLoader: (() -> Void)?
    var onStopLoader: (() -> Void)?
    var onConfirmAction: ((UIAlertController) -> Void)?

    var section: SectionModel = SectionModel(items: [])
    var onAlert: PublishSubject<String> = PublishSubject()

    private var service = RssFeedService(delegate: nil)
    
    let searchView = SearchView(placeHolderText: "Enter feed url")
    
    var feedModel: BehaviorRelay<FeedModel?> = BehaviorRelay(value: nil)
    var rssLink: String = ""
    
    init() {
        section.heightForHeader = 55
        section.viewForHeader = searchView
        
        screenState = BehaviorRelay(value: [section])
        
        service.delegate = self
        
        setupBindables()
    }
    
    private func updateView() {
        section.items.removeAll()
        
        if let feed = feedModel.value {
            let feedCellBindable = FeedCellBindable()
            
            section.items.append(CellType(reuseID: FeedCellTableViewCell.reuseID,
                                          data: [
                                            FeedCellTableViewCell.Params.titleText: feed.title,
                                            FeedCellTableViewCell.Params.descriptionText: feed.description,
                                            FeedCellTableViewCell.Params.imageUrl: feed.image,
                                            FeedCellTableViewCell.Params.hasRightButton: true,
                                            FeedCellTableViewCell.Params.rightButtonImage: UIImage(named: "ic_add_white") as Any
                                          ],
                                          vM: feedCellBindable, height: UITableView.automaticDimension))
            
            feedCellBindable.rightBtnTap.asObservable()
                .subscribe(onNext: { [weak self] in
                    self?.saveFeed(feed: feed)
                }).disposed(by: disposeBag)
        }
        screenState.accept([section])
        
    }
    
    private func saveFeed(feed: FeedModel) {
        let manager = JSONManager(plistName: "savedFeeds")
        var allFeeds: [LocalFeedModel]? = manager.load()
        
        if var tempFeeds = allFeeds {
            if  tempFeeds.contains(where: { $0.link == feed.link }) {
                onAlert.onNext("Feed already added")
            } else {
                tempFeeds.append(LocalFeedModel(with: feed, and: rssLink))
            }
            manager.save(data: tempFeeds)
            showSuccessAlert()
        } else {
            allFeeds?.append(LocalFeedModel(with: feed, and: rssLink))
            manager.save(data: allFeeds ?? [])
            showSuccessAlert()
        }
    }
    
    private func setupBindables() {
        
        feedModel.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.updateView()
            }).disposed(by: disposeBag)
        
        searchView.searchTapped.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let sSelf = self else { return }
                if !sSelf.searchView.searchedText.value.isEmpty {
                    sSelf.rssLink = sSelf.searchView.searchedText.value
                    sSelf.findURL(url: sSelf.searchView.searchedText.value)
                }
            }).disposed(by: disposeBag)
        
        searchView.deleteText.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let sSelf = self else { return }
                sSelf.feedModel.accept(nil)
            }).disposed(by: disposeBag)
    }
    
    private func findURL(url: String) {
        onShowLoader?()
        service.getRSSFeeds(url: url)
    }
    
    private func showSuccessAlert() {
        let actionSheet: UIAlertController = UIAlertController(title: "Feed added", message: "Feed is successfully added", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "OK", style: .default) { _ in }
        actionSheet.addAction(cancelActionButton)
        onConfirmAction?(actionSheet)
    }
}

extension AddFeedViewModel: RssFeedServiceDelegate {
    
    func error(message: String) {
        onStopLoader?()
        onAlert.onNext(message)
    }
    
    func success(data: Any?) {
        onStopLoader?()
        if let response = data as? FeedModel {
            feedModel.accept(response)
        }
    }
    
}
