//
//  FeedsViewController.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

protocol FeedsView: AlertView {
    var onAddNewFeed: (() -> Void)? { get set }
    var onShowItems: ((LocalFeedModel) -> Void)? { get set }
}

class FeedsViewController: RxBaseListViewController, FeedsView, AlertView {
    
    var onAddNewFeed: (() -> Void)?
    var onShowAlerView: ((String) -> Void)?
    var onShowItems: ((LocalFeedModel) -> Void)?
    
    @IBOutlet weak var titleBar: UIView!
    
    var viewModel: FeedsViewModel = FeedsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleBar = CustomTitleBar(
            type: .titleRightButton,
            rightButtonImage: UIImage(named: "ic_add"),
            title: "FEEDS"
        )
        
        titleBar.rightButtonTapped.asObservable().subscribe(onNext: { [weak self] in
            self?.onAddNewFeed?()
        }).disposed(by: disposeBag)
        
        self.titleBar.addSubview(titleBar)
        
        viewModel.onAlert.asObservable().subscribe(onNext: { [weak self] error in
            self?.onShowAlerView?(error)
        }).disposed(by: disposeBag)
        
        viewModel.onShowItems.asObservable()
            .subscribe(onNext: { [weak self] model in
                self?.onShowItems?(model)
            }).disposed(by: disposeBag)
        
        viewModel.onConfirmAction = { [weak self] actionSheet in
            DispatchQueue.main.async {
                self?.present(actionSheet, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.loadLocalFeeds()
    }
    override func baseViewModel() -> RxBaseViewControllerDelegate? {
        return viewModel
    }
}
