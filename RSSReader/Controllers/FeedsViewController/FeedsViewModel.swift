//
//  FeedsViewModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation
import RxCocoa
import RxSwift

class FeedsViewModel: RxBaseViewControllerDelegate {
    
    var disposeBag: DisposeBag = DisposeBag()
    var screenState: BehaviorRelay<[SectionModel]> = BehaviorRelay(value: [])
    var onShowLoader: (() -> Void)?
    var onStopLoader: (() -> Void)?
    var onConfirmAction: ((UIAlertController) -> Void)?

    var onAlert: PublishSubject<String> = PublishSubject()
    var onShowItems: PublishSubject<LocalFeedModel> = PublishSubject()
    var onDeleteFeedInfo: PublishSubject<LocalFeedModel> = PublishSubject()
    var onDelete: PublishSubject<LocalFeedModel> = PublishSubject()

    var section: SectionModel = SectionModel(items: [])
    
    private func deleteFeed(_ feed: LocalFeedModel) {
        let manager = JSONManager(plistName: "savedFeeds")
        let allFeeds: [LocalFeedModel]? = manager.load()
        
        if var feeds = allFeeds {
            feeds.removeAll(where: { $0.id == feed.id })
            manager.save(data: feeds)
            
            loadLocalFeeds()
        }
    }
    
    private func showDeleteConfirmation(for feed: LocalFeedModel) {
        let actionSheet: UIAlertController = UIAlertController(title: "Delete feed", message: "Are you sure you want to delete \"" + feed.title + "\"?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        actionSheet.addAction(cancelActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Delete", style: .default) { _ in
            self.deleteFeed(feed)
        }
        actionSheet.addAction(deleteActionButton)
        
        onConfirmAction?(actionSheet)
    }
    
    func loadLocalFeeds() {
        section.items.removeAll()
        
        let manager = JSONManager(plistName: "savedFeeds")
        let allFeeds: [LocalFeedModel]? = manager.load()
        
        if let feeds = allFeeds {
            for feed in feeds {
                let feedCellBindable = FeedCellBindable()
                
                section.items.append(CellType(reuseID: FeedCellTableViewCell.reuseID,
                                              data: [
                                                FeedCellTableViewCell.Params.titleText: feed.title,
                                                FeedCellTableViewCell.Params.descriptionText: feed.description,
                                                FeedCellTableViewCell.Params.imageUrl: feed.image,
                                                FeedCellTableViewCell.Params.hasRightButton: true,
                                                FeedCellTableViewCell.Params.rightButtonImage: UIImage(named: "ic_delete") as Any
                                              ],
                                              vM: feedCellBindable, height: UITableView.automaticDimension))
                
                feedCellBindable.rightBtnTap.asObservable()
                    .subscribe(onNext: { [weak self] in
                        self?.showDeleteConfirmation(for: feed)
                    }).disposed(by: disposeBag)
                
                feedCellBindable.cellTap.asObservable()
                    .subscribe(onNext: { [weak self] in
                        self?.onShowItems.onNext(feed)
                    }).disposed(by: disposeBag)
            }
        }
        
        screenState.accept([section])
    }
}
