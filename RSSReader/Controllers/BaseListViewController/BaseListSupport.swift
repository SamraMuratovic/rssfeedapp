//
//  BaseListSupport.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit

protocol Reusable {}

protocol Configurable {
    func configureCell(withData data: [String: Any])
    func configureCell(withViewModel: Any?)
}

extension UITableViewCell: Reusable, Configurable {
    @objc func configureCell(withData data: [String: Any]) {}
    @objc func configureCell(withViewModel: Any?) {}
}

extension Reusable where Self: UITableViewCell {
    static var reuseID: String {
        return String(describing: self)
    }
}

extension UITableView {
    func registerCell<Cell: UITableViewCell> (_ cellClass: Cell.Type) {
        register(UINib(nibName: cellClass.reuseID, bundle: nil), forCellReuseIdentifier: cellClass.reuseID)
    }
    
    func registerClass<Cell: UITableViewCell> (_ cellClasss: Cell.Type) {
        register(cellClasss, forCellReuseIdentifier: cellClasss.reuseID)
    }
    
    func dequeueReusableCell<Cell: UITableViewCell>(for indexPath: IndexPath) -> Cell {
        guard let cell = self.dequeueReusableCell(withIdentifier: Cell.reuseID, for: indexPath) as? Cell else {
            fatalError("Fatal error for cell at \(indexPath)")
        }
        return cell
    }
}
