//
//  RxBaseListViewController.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import NVActivityIndicatorView

// MARK: - SectionModel
struct SectionModel: AnimatableSectionModelType {
    
    static func == (lhs: SectionModel, rhs: SectionModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    var identity: String {
        return id
    }
    
    var id: String = ""
    var heightForHeader: CGFloat
    var viewForHeader: UIView?
    var items: [CellType]
    
    init(id: String? = nil, items: [CellType], heightForHeader: CGFloat = 0.0, viewForHeader: UIView? = nil) {
        self.items = items
        self.heightForHeader = heightForHeader
        self.viewForHeader = viewForHeader
        self.id = id ?? UUID().uuidString
    }
}

extension SectionModel: SectionModelType {
    typealias Item = CellType
    
    init(original: SectionModel, items: [CellType]) {
        self = original
        self.items = items
    }
}

// MARK: - CellDelegate
protocol CellDelegate {}

// MARK: - CellTypeDelegate
protocol CellTypeDelegate: IdentifiableType {
    var params: [String: Any]? { get }
    var vM: CellDelegate? { get }
    var heightForRow: CGFloat { get }
    init(id: String?, reuseID: String, data: [String: Any]?, vM: CellDelegate?, height: CGFloat)
}

// MARK: - CellType
struct CellType: CellTypeDelegate, Equatable {
    
    static func == (lhs: CellType, rhs: CellType) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id: String
    var identity: String {
        return id
    }
    
    var params: [String: Any]?
    var vM: CellDelegate?
    var heightForRow: CGFloat
    var reuseID: String
    
    init(id: String? = nil, reuseID: String, data: [String: Any]? = nil, vM: CellDelegate? = nil, height: CGFloat = CGFloat(UITableView.automaticDimension)) {
        self.id = id ?? UUID().uuidString
        self.reuseID = reuseID
        self.params = data
        self.vM = vM
        self.heightForRow = height
    }
}
// MARK: - BaseListViewControllerDelegate
protocol RxBaseViewControllerDelegate: AnyObject {
    var disposeBag: DisposeBag { get }
    var screenState: BehaviorRelay<[SectionModel]> { get }
    var onShowLoader: (() -> Void)? { get set }
    var onStopLoader: (() -> Void)? { get set }
}

class RxBaseListViewController: BaseViewController, NVActivityIndicatorViewable {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var keyboardShown: PublishSubject<Void> = PublishSubject()
    var useAnimations: Bool = false
    var animationConfiguration: AnimationConfiguration?
    
    // MARK: - Private
    public let disposeBag = DisposeBag()
    private var _dataSource: RxTableViewSectionedAnimatedDataSource<SectionModel>?

    // MARK: - ViewModel
    public func baseViewModel() -> RxBaseViewControllerDelegate? {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.bounces = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(forceEndEditing), name: Notification.Name("endEditing"), object: nil)
        
        registerCell()
        
        let dataSource = RxTableViewSectionedAnimatedDataSource<SectionModel>(
            configureCell: { (dataSource, tableView, indexPath, item) in
                let cell = tableView.dequeueReusableCell(withIdentifier: item.reuseID, for: indexPath)
                if let params = dataSource[indexPath].params {
                    cell.configureCell(withData: params)
                }
                if let vM = dataSource[indexPath].vM {
                    cell.configureCell(withViewModel: vM)
                }
                return cell
        })
        
        if !useAnimations {
            dataSource.decideViewTransition = { (_, _, _)  in return RxDataSources.ViewTransition.reload
            }
        }
        
        if let animationConfiguration = animationConfiguration {
            dataSource.animationConfiguration = animationConfiguration
        }
        
        self._dataSource = dataSource
        
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        
        baseViewModel()?.screenState.asObservable()
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        guard let vM = baseViewModel() else { return }
        
        vM.onStopLoader = { [weak self] in
            self?.hideLoader()
        }
        vM.onShowLoader = { [weak self] in
            self?.showLoader()
        }
    }
    
    // MARK: Keyboard handling
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 10.0, right: 0)
            tableView.contentInset = contentInsets
            tableView.scrollIndicatorInsets = contentInsets
            keyboardShown.onNext(())
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        tableView.contentInset = .zero
        tableView.scrollIndicatorInsets = .zero
    }
    
    @objc func forceEndEditing() {
        self.view.endEditing(true)
    }
}

extension RxBaseListViewController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let dataSource = _dataSource, let viewForHeader = dataSource.sectionModels[section].viewForHeader else {
            return nil
        }
        return viewForHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let dataSource = _dataSource else {
            return 0.0
        }
        return dataSource.sectionModels[section].heightForHeader
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let dataSource = _dataSource else {
            return 0.0
        }
        return dataSource.sectionModels[indexPath.section].items[indexPath.row].heightForRow
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - Private func
    private func registerCell() {
        tableView.registerCell(FeedCellTableViewCell.self)
        tableView.registerCell(ImageCell.self)
    }
}
