//
//  FeedModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 6. 7. 2021..
//

import Foundation

struct FeedModel {
    
    var title: String
    var link: String
    var description: String
    var image: String
    var items: [ItemModel]?

    func isEmpty() -> Bool {
        return title.isEmpty && link.isEmpty && description.isEmpty && image.isEmpty
    }
    
}
