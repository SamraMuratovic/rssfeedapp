//
//  LocalFeedModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation

struct LocalFeedModel: Codable {
    
    var id: UUID = UUID()
    var title: String
    var link: String
    var description: String
    var image: String
    var rssLink: String
    
    init(with feed: FeedModel, and rssURL: String) {
        id = UUID()
        title = feed.title
        link = feed.link
        description = feed.description
        image = feed.image
        rssLink = rssURL
    }
}
