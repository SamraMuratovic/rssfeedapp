//
//  AlertDataModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation
import RxSwift

struct AlertBindables: CellDelegate {
    var actionPressed: PublishSubject<Void> = PublishSubject()
}

struct AlertDataModel {
    var title: String? = "Error"
    var infoMessage: String? = ""
    var bindable: AlertBindables?
    var actions: [Action] = [Action()]
    
    struct Action {
        var name: String = "OK"
    }
    
    func actionTapped() {
        if let bindable = bindable {
            bindable.actionPressed.onNext(())
        }
    }
}
