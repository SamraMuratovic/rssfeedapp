//
//  ItemModel.swift
//  RSSReader
//
//  Created by Samra Muratovic on 11. 7. 2021..
//

import Foundation

struct ItemModel {
    
    var title: String
    var link: String
    var description: String
    var image: String
}
